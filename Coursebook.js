class Coursebook extends React.Component {
  state = {
    items: [
      {
        id: 1,
        name: "JSX - JavaScript Syntax Extension - pełni rolę preprocesora, Babel przekłada go na czysty JavaScript",
        active: true,
      },
      {
        id: 2,
        name: "Przy JSX nalezy uważać na nawiasy, aby kompilator nie wrzucił ;",
        active: true,
      },
      {
        id: 3,
        name: ` W React w JSX jak tworzymy jakiś element musi być on zawarty w
            jednym elemencie nadrzędnym. React pozwala na korzystanie z 
           <React.Fragment></React.Fragment> / <> </>`,
        active: true,
      },
      {
        id: 4,
        name: `Tworząc element w React wywołujemy metodę React.createElement(type,props,children) - props przekazywany jest jako 
      obiekt`,
        active: true,
      },
      {
        id: 5,
        name: "W JSX za pomocą {} przekazujemy wyrażenie JS",
        active: true,
      },
      {
        id: 6,
        name: "Element React to najmniejszy blok aplikacji w React Elementy sa obiektami",
        active: true,
      },
      {
        id: 7,
        name: (
          <span>
            Komponenty tworzą strukturę aplikacji w oparciu o elementy <br />
            React Elementy React nie są modyfikowane tylko w przypadku jakiejś
            zmiany są renderowane na nowo <br />
            Na podstawie elementów React tworzony jest virtual DOM!! <br />
            Renderowanie strony: <br />
            1. podczas pierwszego uruchomienia renderowany jest DOM <br />
            2. następuje zmiana na stronie <br />
            3. porównanie wygenerowanego virtual DOM przed i po zmianie i
            wysłanie do przeglądarki tylko tego co uległo zmianie
          </span>
        ),
        active: true,
      },
      {
        id: 8,
        name: "Połączenie virtual DOM z DOM odbywa się za pomocą biblioteki React DOM i z niej korzystamy z metody render",
        active: true,
      },
      {
        id: 9,
        name: (
          <span>
            virtual DOM cechy:
            <br />- szybki wydajny dynamiczny interfejs
            <br />- renderowane są tylko te elemnty które się zmieniły
            <br />- oparty na elementach React
          </span>
        ),
        active: true,
      },
      {
        id: 10,
        name: "Komponent to podstawowy element interfejsu",
        active: true,
      },
      {
        id: 11,
        name: "Wyróżniamy dwa typy: komponenty stanowe(klasowe) i bezstanowe(funkcyjne)",
        active: true,
      },
      {
        id: 12,
        name: (
          <span>
            Cechy:
            <br />
            hermetyczny
            <br />
            możliwość zagnieżdzania w innych komponentach
            <br />
            możliwośc używania wilokrotnego
            <br />
            zbudowane z logiki i struktury
            <br />
            zwraca zawartość i umożliwia re-renderowanie
          </span>
        ),
        active: true,
      },
      {
        id: 13,
        name: "Przepływ danych w React jest od komponentu nadrzędnego do podrzędnego. Związane z tym jest renderowanie, które odbywa się z góry na dół.",
        active: true,
      },
      {
        id: 14,
        name: (
          <pre>
            Komponent funkcyjny Tworzenie const ShoppingList=()=return( i tutaj
            jakiś kod opisujący element) const ShoppingList=()=( i tutaj jakiś
            kod opisujący element) - zapis skrócony jeżeli ammy tylko strukturę
            bez logiki
          </pre>
        ),
        active: true,
      },
      {
        id: 15,
        name: (
          <span>
            Cechy:
            <br />- nie posiada stanu
            <br />- może przyjmowac props
          </span>
        ),
        active: true,
      },
      {
        id: 16,
        name: (
          <span>
            Komponent stanowy
            <br />
            Cechy:
            <br />- posiada stan
            <br />- posiada cykl życia
            <br />- powniene być używany tylko gdy potrzebujemy stanu lub cyklu
            życia
            <br />- wymaga użycia merody render()
          </span>
        ),
        active: true,
      },
      {
        id: 17,
        name: "Wartości state wykorzystywane w komponencie klasowym możemy startowo zadeklarowac w konstruktorze lub dalej w klasie",
        active: true,
      },
      {
        id: 18,
        name: "Jeżeli chcemy uzyć własnego konstruktora używamy super() - wywołanie konstruktora klasy nadrzędnej",
        active: true,
      },
      {
        id: 19,
        name: "Props - obiekt przechowujący dane przekazane do komponentu. Tylko do odczytu",
        active: true,
      },
      {
        id: 20,
        name: "State - obiekt należący do komponentu klasowego, jego zmiana powiduje aktualizację komponentu.",
        active: true,
      },
      {
        id: 21,
        name: (
          <span>
            State nawet jak nie stworzymy to istnieje
            <br />
            Stan możemy utworzyć:
            <br />- w konstruktorze
            <br />- bezpośrednio w klasie
            <br />
            Stan jest przypisany do stworzonej instancji komponentu
            <br />
            Do zarządzenia stanem State wykorzystujemy metodę setState -
            odpowiada ona za ponowne renderowanie komponentów
            <br />
            Możemy do setState przekazać ()= i w niej np prevState dać żeby coś
            zmienić
          </span>
        ),
        active: true,
      },
      {
        id: 22,
        name: (
          <span>
            Props
            <br />
            Obiekt przechowujący dane i pozwalający na przekazywanie ich
            pomiedzy komponentami (przepływ od rodzica do dziecka)
            <br />
            Obiekt props znajduje się w każdym komponencie. Jeżeli nie
            przekażemy w nim atrybutów to jest pusty.
            <br />
            State też możemy przekazać jako props
            <br />
            Props jest tylko do odczytu, nie zmieniamy w nim nic.
            <br />W komponencie klasowym do props odnosimy się przez
            this.props.nazwa_właściwości
            <br />W komponencie funkcyjnym do props odnosimy się
            props.nazwa_właściwości
          </span>
        ),
        active: true,
      },
      {
        id: 23,
        name: "W klasie domyślne wartości definiujemy w postaci wartości statycznej defaultProps",
        active: true,
      },
      {
        id: 24,
        name: "W komponencie funkcyjnym to samo robimy przez zadeklarowanie nazwa_komponentu.defaultProps",
        active: true,
      },
      {
        id: 25,
        name: (
          <span>
            Kiedy używać komponentów stanowych:
            <br />- potrzeba stanu <br />- wykorzystujemy cykl życia komponentu{" "}
            <br />- wykorzystujemy funkcje do obsługi zdarzeń bezpośrednio w
            komponencie
            <br />W pozostałych przypadkach komponenty funkcyjne
          </span>
        ),
        active: true,
      },
      {
        id: 26,
        name: (
          <span>
            REACT EVENTY
            <br />
            React przy każdym zdarzeniu tworzy synthetic event co jest
            odpowiednikiem event w DOM (click, scroll itd.).
          </span>
        ),
        active: true,
      },
      {
        id: 27,
        name: (
          <span>
            THIS - wiązanie
            <br />
            1. bind - użycie w deklaracji eventu, bind przypisuje this z
            nadkomponentu do środka funkcji. Bind powoduje utworzenie nowej
            funkcji o tej samej nazwie. input onChange=this.
            nazwa_funkcji.bind(this) -
            <br />
            2. w konstruktorze, każda instancja będzie miała swoją funkcję z
            przypisanym this this.handle = this. nazwa_funkcji.bind(this)
            <br />
            3. funkcja strzałkowa - nie tworzy swojego this tylko dziedziczy go.
            <br />
            4. w evencie przekazanie anonimowej funkcji strzałkowej i w niej
            metoda właściwa. input onChange=()=this. nazwa_funkcji() - za kazym
            razem tworzymy nową funkcję, można w wywołaniu przekazywac argumenty
            <br />
            5. deklaracja metody i przypisanie jej do własciwości
          </span>
        ),
        active: true,
      },
      {
        id: 28,
        name: (
          <span>
            Tablice
            <br />
            Atrybut key dla elementu przekazujmy tam gdzie jest tworzony element
            np. przy metodzie .map
            <br />
            Kopię tablicy możemy wykonać po przez:
            <br />- ...
            <br />- slice
            <br />- Array.from(nazwa_tablicy)
          </span>
        ),
        active: true,
      },
      {
        id: 29,
        name: (
          <span>
            Formularze
            <br />
            Istnieją komponenty kontrolowane i niekontrolowane
            <br />
            Kontrolowany:
            <br />- wykorzystujemy state do trzymania stanu formularza
            <br />- wykorzystujemy value
            <br />- zmiana state odywa się za pomocą onChange
          </span>
        ),
        active: true,
      },
    ],
  };

  handleChangeStatus = (id) => {
    const items = this.state.items.map((item) => {
      if (id === item.id) {
        item.active = !item.active;
      }
      return item;
    });

    this.setState({
      items,
    });
  };

  render() {
    return (
      <React.Fragment>
        <ListItems
          items={this.state.items}
          changeStatus={this.handleChangeStatus}
        />
      </React.Fragment>
    );
  }
}
